# Information

L'application utilise sqlite3 pour avoir une persistance de données

# Acces à l'api swagger

L'API REST est accessible via une interface swagger
http://localhost:8055/api

## Available Scripts

### `npm start`

Lance l'application en mode autonome

### `run.bat`

Lance l'application