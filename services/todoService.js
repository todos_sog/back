const sqlite3 = require("sqlite3").verbose();
const db = new sqlite3.Database("./todo.db");

exports.getTodos = (req, res) => {
  db.all("SELECT * FROM todos", (err, rows) => {
    if (err) {
      return res.status(500).json({ error: err.message });
    }
    res.json(rows);
  });
};

exports.getTodoById = (req, res) => {
  const id = req.params.id;
  db.get("SELECT * FROM todos WHERE id = ?", id, (err, row) => {
    if (err) {
      return res.status(500).json({ error: err.message });
    }
    if (!row) {
      return res.status(404).json({ error: "Todo not found." });
    }
    res.json(row);
  });
};

exports.addTodo = (req, res) => {
  const { title, description, completed } = req.body;
  if (!title) {
    return res.status(400).json({ error: "Title is required." });
  }
  db.run(
    "INSERT INTO todos (title, description, completed) VALUES (?, ?, ?)",
    title,
    description,
    completed,
    (err) => {
      if (err) {
        return res.status(500).json({ error: err.message });
      }
      res.json({ message: "Todo created successfully." });
    }
  );
};

exports.updateTodo = (req, res) => {
  const { id, title, description, completed } = req.body;
  const todoData = [title, description, completed, id];
  db.get("SELECT * FROM todos WHERE id = ?", id, (err, row) => {
    if (err) {
      return res.status(500).json({ error: err.message });
    }
    if (!row) {
      return res.status(404).json({ error: "Todo not found." });
    }
    db.run(
      "UPDATE todos SET title = ?, description = ?, completed = ? WHERE id=?",
      todoData,
      (err) => {
        if (err) {
          return res.status(500).json({ error: err.message });
        }
        res.json({ message: "Todo updated successfully." });
      }
    );
  });
};

exports.deleteTodo = (req, res) => {
  const id = req.params.id;
  db.get("SELECT * FROM todos WHERE id = ?", id, (err, row) => {
    if (err) {
      return res.status(500).json({ error: err.message });
    }
    if (!row) {
      return res.status(404).json({ error: "Todo not found." });
    }
  });
  db.run("DELETE FROM todos WHERE id = ?", id, (err, rows) => {
    if (err) {
      return res.status(500).json({ error: "Error deleting todo item" });
    }
    return res.json({ message: `Todo with id: ${id} has been deleted.` });
  });
};
