const express = require("express");
const router = require("./router");
const cors = require("cors");
const sqlite3 = require("sqlite3").verbose();

const db = new sqlite3.Database("./todo.db");
// Create the table for storing todos items
db.serialize(() => {
  db.run(`CREATE TABLE IF NOT EXISTS todos (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        title TEXT NOT NULL,
        description TEXT,
        completed BOOLEAN DEFAULT false,
        updated_at DATETIME DEFAULT CURRENT_TIMESTAMP
      )`);
  db.close();
});

const app = express();
app.use(
  cors({
    origin: "*",
  })
);

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use("/", router);

app.listen(8055, () => {
  console.log("API listening on port 8055");
});
