const express = require("express");
const swaggerJsDoc = require("swagger-jsdoc");
const swaggerUi = require("swagger-ui-express");

// eslint-disable-next-line new-cap
const router = express.Router();

const todoService = require("./services/todoService");

const swaggerOptions = {
  definition: {
    openapi: "3.0.0",
    info: {
      title: "Todo List Rest API",
      version: "1.0.0",
      description: "Amazing Todo List API.",
      contact: {
        name: "Sogeti",
      },
      servers: ["http://localhost:8055"],
    },
  },
  apis: ["router.js"], // files containing annotations as above
};

const swaggerDocs = swaggerJsDoc(swaggerOptions);
router.use("/api", swaggerUi.serve, swaggerUi.setup(swaggerDocs));

// Add a todo item
/**
 * @openapi
 * /todos:
 *   post:
 *     description: Add Todos in database.
 *     produces:
 *       - application/json
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             description: title todo.
 *             type: object
 *             required:
 *               - title
 *             properties:
 *               title:
 *                 type: string
 *               description:
 *                 type: string
 *               completed:
 *                 type: boolean
 *                 default: false
 *     responses:
 *       200:
 *         description: Return (JSON) an todo by his id.
 *       400:
 *        description: Todo is required.
 *       500:
 *         description: Error message.
 */
router.post("/todos", todoService.addTodo);

// Get all todo items
/**
 * @openapi
 * /todos:
 *   get:
 *     description: Returns all todos in database.
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Returns (JSON) all todos in database.
 *       500:
 *         description: Internal error.
 */
router.get("/todos", todoService.getTodos);

// Get a specific todo item by id
/**
 * @openapi
 * paths:
 *   /todos/{id}:
 *     get:
 *       description: Get todo by his id.
 *       produces:
 *         - application/json
 *       parameters:
 *         - in: path
 *           name: id
 *           schema:
 *             type: integer
 *           requires: true
 *           description: Todo id
 *       responses:
 *         200:
 *           description: Return (JSON) an todo by his id.
 *         404:
 *           description: User Not Found.
 *         500:
 *           description: Internal error.
 */

router.get("/todos/:id", todoService.getTodoById);

// Update a specific todo item by id
/**
 * @openapi
 * /todos:
 *   put:
 *     description: Update Todo in database.
 *     produces:
 *       - application/json
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             description: Update todo if exists.
 *             type: object
 *             required:
 *               - id
 *             properties:
 *               id:
 *                 type: integer
 *               title:
 *                 type: string
 *               description:
 *                 type: string
 *               completed:
 *                 type: boolean
 *                 default: false
 *     responses:
 *       200:
 *         description: Todo updated successfully.
 *       500:
 *         description: Internal error.
 */
router.put("/todos", todoService.updateTodo);

// Delete a specific todo item by id
/**
 * @openapi
 * paths:
 *   /todos/{id}:
 *     delete:
 *       description: Delete todo by his id.
 *       produces:
 *         - application/json
 *       parameters:
 *         - in: path
 *           name: id
 *           schema:
 *             type: string
 *           requires: true
 *           description: Todo id
 *       responses:
 *         200:
 *           description: Return (JSON) an user by his id.
 *         404:
 *           description: Todo Not Found.
 *         500:
 *           description: Internal error.
 */
router.delete("/todos/:id", todoService.deleteTodo);

router.use((req, res) => {
  res.status(404).send("Request does not exists");
});

module.exports = router;
